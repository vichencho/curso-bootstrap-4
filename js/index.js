/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



$(function(){
    $("[data-toggle='tooltip']").tooltip();
});
$(function(){
    $("[data-toggle='popover']").popover();
});
$('.carousel').carousel({
    interval: 2000 
});

$('#contacto').on('show.bs.modal', function (e) {
    console.log('el modal contacto se esta mostrando');
    $('#contactoBtn').prop('disabled',true);
    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-primary');
});

$('#contacto').on('shown.bs.modal', function (e) {
    console.log('el modal contacto se mostró');
});

$('#contacto').on('hide.bs.modal', function (e) {
    console.log('el modal contacto se esta ocultando');
});

$('#contacto').on('hidden.bs.modal', function (e) {
    console.log('el modal contacto se ocultó');
    $('#contactoBtn').prop('disabled',false);
    $('#contactoBtn').removeClass('btn-primary');
    $('#contactoBtn').addClass('btn-outline-success');
});